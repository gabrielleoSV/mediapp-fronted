import { Injectable } from '@angular/core';
import { HOST } from '../_shared/var.constant';
import { Especialidad } from '../_model/especialidad';
import { HttpClient } from '@angular/common/http';

@Injectable()
export class EspecialidadService {
  url :string =  `${HOST}/especialidad`;
  constructor(private http: HttpClient) { }

  getListaEspecialidad(){
    return this.http.get<Especialidad[]>(`${this.url}/listar/`);
  }
}
