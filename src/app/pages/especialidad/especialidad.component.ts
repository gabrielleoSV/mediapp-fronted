import { Component, OnInit, ViewChild } from '@angular/core';
import { Especialidad } from '../../_model/especialidad';
import { MatPaginator, MatSort, MatTableDataSource } from '@angular/material';
import { EspecialidadService } from '../../_service/especialidad.service';

@Component({
  selector: 'app-especialidad',
  templateUrl: './especialidad.component.html',
  styleUrls: ['./especialidad.component.css']
})
export class EspecialidadComponent implements OnInit {
  listaEspecialidades : Especialidad[] = [];
  displayedColumns = ['idEspecialidad','nombre','acciones'];
  dataSource: MatTableDataSource<Especialidad>;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  constructor(private especialidadService : EspecialidadService) { }

  ngOnInit() {
    this.especialidadService.getListaEspecialidad().subscribe(data =>{
        this.listaEspecialidades = data;
        console.log(this.listaEspecialidades);
    });

    setTimeout(() => {
      this.dataSource = new MatTableDataSource(this.listaEspecialidades);
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;
    }, 1000);
  }

  applyFilter(filterValue: string) {
    filterValue = filterValue.trim(); // Remove whitespace
    filterValue = filterValue.toLowerCase(); // Datasource defaults to lowercase matches
    this.dataSource.filter = filterValue;
  }
}
